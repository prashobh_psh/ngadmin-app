import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { BeforeAuthGuard } from './auth/before-auth.guard';

const routes: Routes = [
  { path: '', canActivate: [BeforeAuthGuard], loadChildren: './auth/auth.module#AuthModule' },
  { path: '', canActivate: [AuthGuard], loadChildren: './dashboard/dashboard.module#DashboardModule' },
  // { path: '', loadChildren: './error/error.module#ErrorModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
