import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginFrm: FormGroup;
  isFormSubmitted: boolean = false;
  loginSubscribe: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private title: Title
  ) { }

  /**
   * Set page title
   */
  ngOnInit() {
    this.title.setTitle(`${environment.appTitle} - Login`);
    this.buildForm();
  }

  /**
   * Build login form
   */
  buildForm() {
    this.loginFrm = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, Validators.required],
    })
  }

  /**
   * Get login form controls
   */
  get formControls() {
    return this.loginFrm.controls;
  }

  /**
   * On login submit
   */
  login() {
    this.isFormSubmitted = true;
    if (this.loginFrm.invalid)
      return;

    let query = {
      username: this.formControls.username.value,
      password: this.formControls.password.value,
    }
    this.loginSubscribe = this.authService.login(query).subscribe((data: any) => {
      this.authService.saveAuth(data.token);
      this.router.navigate(['clients']);
    }, (error: any) => {
      console.log(error);
    });
  }

  /**
   * On component destroy
   */
  ngOnDestroy() {
    if (this.loginSubscribe)
      this.loginSubscribe.unsubscribe();
  }

}
