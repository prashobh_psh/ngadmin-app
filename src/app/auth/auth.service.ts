import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Get access token
   */
  getToken() {
    return localStorage.getItem('token');
  }

  /**
   * Save token in localstorage
   * 
   * @param token 
   */
  saveAuth(token: string) {
    localStorage.setItem("token", token);
  }

  /**
   * Clear auth data
   */
  private clearAuthData() {
    localStorage.removeItem("token");
  }

  /**
   * Logout
   */
  signout() {
    this.clearAuthData();
  }

  /**
   * Login
   */
  login(query: any) {
    return this.http.post<any>(`${environment.appUrl}api/token-auth/`, query);
  }
}
