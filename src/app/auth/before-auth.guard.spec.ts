import { TestBed, async, inject } from '@angular/core/testing';

import { BeforeAuthGuard } from './before-auth.guard';

describe('BeforeAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeforeAuthGuard]
    });
  });

  it('should ...', inject([BeforeAuthGuard], (guard: BeforeAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
