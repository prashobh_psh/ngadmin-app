import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from '../../dashboard.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

  sidebarMenuList: any = [];
  selectedMenu: any = null;

  constructor(
    private dashboardService: DashboardService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.sidebarMenuList = this.dashboardService.sidebarMenu;
  }

  /**
   * Toggle menu
   * 
   * @param menu 
   */
  toggleMenu(menu: any) {
    if (this.selectedMenu == menu.id) {
      this.selectedMenu = null;
    } else {
      this.selectedMenu = menu.id;
    }
    if (menu.url != '') {
      this.router.navigate([`${menu.url}`]);
    }
  }

}
