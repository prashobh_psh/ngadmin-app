import { Component, OnInit, Renderer, AfterViewInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

declare const $: any;

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {

  smallMenu: boolean = false;
  toggleMsg: boolean = false;
  toggleNoti: boolean = false;
  toggleAcc: boolean = false;

  constructor(
    private renderer: Renderer,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit() { }

  /**
   * Toggle menu
   */
  toggleMenu() {
    this.smallMenu = !this.smallMenu;
    if (this.smallMenu) {
      this.renderer.setElementClass(document.body, 'sidebar-icon-only', true);
    } else {
      this.renderer.setElementClass(document.body, 'sidebar-icon-only', false);
    }
  }

  /**
   * Toggle menu
   */
  toggleMessage() {
    this.toggleMsg = !this.toggleMsg;
  }

  /**
   * Toggle notification
   */
  toggleNotification() {
    this.toggleNoti = !this.toggleNoti;
  }

  /**
   * Toggle account
   */
  toggleAccount() {
    this.toggleAcc = !this.toggleAcc;
  }

  /**
   * Logout
   */
  logout() {
    this.authService.signout();
    this.router.navigate(['login']);
  }

  ngAfterViewInit() {
    $('body').click((e: any) => {
      var target = $(e.target);
      if (!target.parents('.mr-1').length) {
        this.toggleMsg = false;
      }
      if (!target.parents('.mr-4').length) {
        this.toggleNoti = false;
      }
      if (!target.parents('.nav-profile').length || !target.parents('.account-menu-toggle').length) {
        this.toggleAcc = false;
      }
    });
  }

}
