import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profileForm: FormGroup;
  isFormSubmitted: boolean = false;

  constructor(
    private title: Title,
    private formBuilder: FormBuilder,
  ) { }

  /**
   * Set page title
   * Build form
   */
  ngOnInit() {
    this.title.setTitle(`${environment.appTitle} - Profile`);
    this.buildForm();
  }

  /**
   * Build form
   */
  buildForm() {
    this.profileForm = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
    });
  }

  /**
   * Get formcontrols
   */
  get formControls() {
    return this.profileForm.controls;
  }

  /**
   * Update form
   */
  updateProfile() {
    this.isFormSubmitted = true;
    if (this.profileForm.invalid) {
      return;
    }
  }

  /**
   * Reset form
   */
  cancel() {
    this.isFormSubmitted = false;
    this.profileForm.reset();
  }

}
