import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  rules: any = [
    { "weight": 0.5, "rule_number": 1, "description": "", "display_name": "Backdated JE" },
    { "weight": 2.5, "rule_number": 2, "description": "", "display_name": "Adjustment/Write-Off Entries" },
    { "weight": 3.5, "rule_number": 3, "description": "", "display_name": "Allowances" },
    { "weight": 4.5, "rule_number": 4, "description": "", "display_name": "Rounded Off Entries" },
    { "weight": 5.0, "rule_number": 5, "description": "", "display_name": "High Risk Accounts" }];

  systemItems: any = [
    { id: 1, name: 'Adjustments & Write off' },
    { id: 2, name: 'Allowances' },
    { id: 3, name: 'Rounded off entries' },
  ];

  constructor() { }

  /**
   * Get rule list
   */
  ruleList() {
    return this.rules;
  }

  /**
   * Get system items
   */
  getSystemItems() {
    return this.systemItems;
  }
}
