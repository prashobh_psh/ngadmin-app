import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng5SliderModule } from 'ng5-slider';
import { Select2Module } from 'ng2-select2';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

import { SettingsRoutingModule } from './settings-routing.module';
import { ListComponent } from './list/list.component';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    Ng5SliderModule,
    FormsModule,
    AutocompleteLibModule,
    Select2Module,
    ReactiveFormsModule,
    SettingsRoutingModule
  ]
})
export class SettingsModule { }
