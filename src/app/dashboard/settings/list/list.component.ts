import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Options } from 'ng5-slider';
import { Select2OptionData } from 'ng2-select2';
import { environment } from '../../../../environments/environment';
import { SettingsService } from '../settings.service';
import { ClientService } from '../../client/client.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('auto', { static: false }) auto: any;

  clients: any = [];
  keyword = 'name';
  // clients: Array<Select2OptionData> = [];
  // public clientsOptions: Select2Options;
  // clientsInitialValue = '';
  // public clientsAjaxOptions: Select2AjaxOptions;
  selectedClientId: any = null;
  paramSubscribe: Subscription;
  clientSubscribe: Subscription;

  gls: Array<Select2OptionData> = [
    { id: "gls1", text: 'gls1' },
    { id: "gls2", text: 'gls2' },
    { id: "gls3", text: 'gls3' },
    { id: "gls4", text: 'gls4' },
    { id: "gls5", text: 'gls5' },
  ];
  public glsOptions: Select2Options;
  glsInitialValue = '';

  currentTab: string = 'system';
  currentMenu: any = null;
  rules: any = [];
  items: any = [];
  options: Options = {
    floor: 0,
    ceil: 10
  };

  constructor(
    private title: Title,
    private route: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private settingsService: SettingsService,
    private clientService: ClientService,
  ) { }

  /**
   * Set page title
   * Load all clients
   * Load GLS
   * Load rules
   * Load system items
   */
  ngOnInit() {
    this.title.setTitle(`${environment.appTitle} - Settings`);
    this.paramSubscribe = this.route.queryParams.subscribe((param: any) => {
      if (param['name']) {
        this.selectedClientId = param['name'];
        setTimeout(() => {
          this.selectedClientId = param['name'];
        }, 200)
      }

      this.loadGls();
      this.loadRules();
      this.loadSystemItems();
    });
  }

  /**
   * On item select event
   * 
   * @param item 
   */
  selectEvent(item: any) {
    this.selectedClientId = item.id;
  }

  /**
   * On clear
   */
  onClear() {
    this.selectedClientId = null;
  }

  /**
   * On search change event
   * 
   * @param keyword 
   */
  onChangeSearch(keyword: string) {
    this.clients = this.clientService.clientList();
  }

  /**
   * Load GLS
   */
  loadGls() {
    this.glsOptions = {
      // allowClear: true,
      multiple: true,
      placeholder: 'GLS'
    }
  }

  /**
   * gls change event
   * 
   * @param data 
   */
  glsChange(data: { value: string[] }) {
    console.log(data.value);
  }

  /**
   * Load system items
   */
  loadSystemItems() {
    this.items = this.settingsService.getSystemItems();
  }

  /**
   * Load rules
   */
  loadRules() {
    this.rules = this.settingsService.ruleList();
  }

  /**
   * Toggle tab
   * 
   * @param type 
   */
  toggleTab(type: string) {
    this.currentTab = type;
  }

  /**
   * Expand collapse menu
   * 
   * @param item 
   */
  expandCollapse(item: any) {
    if (this.currentMenu == item.id) {
      this.currentMenu = null;
    } else {
      this.currentMenu = item.id;
    }
  }

  /**
   * Save rules
   */
  saveRules() {

  }

  /**
   * Save system settings
   */
  saveSystemSettings() {

  }

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
  }

  /**
   * On component destroy
   */
  ngOnDestroy() {
    if (this.clientSubscribe)
      this.clientSubscribe.unsubscribe();
  }

}
