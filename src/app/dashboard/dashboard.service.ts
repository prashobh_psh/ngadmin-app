import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  menuItems: any = [
    { id: 'clients', title: 'Clients', icon: 'mdi mdi-account-multiple menu-icon', url: 'clients' },
    { id: 'visualization', title: 'Visualization', icon: 'mdi mdi-chart-bar-stacked menu-icon', url: 'clients' },
    { id: 'settings', title: 'Settings', icon: 'mdi mdi-settings menu-icon', url: 'settings' },
  ];

  constructor() { }

  /**
   * Get sidebar menu list
   */
  get sidebarMenu() {
    return this.menuItems;
  }
}
