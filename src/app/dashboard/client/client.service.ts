import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  clients: any = [
    {
      id: 1,
      name: "Client 1",
      user: 1,
      short_name: "",
      username: "client1",
      email: "client1@mail.com",
      status: "active",

    },
    {
      id: 2,
      name: "Client 2",
      user: 2,
      short_name: "",
      username: "client2",
      email: "client2@mail.com",
      status: "inactive",
    }
  ]

  clientsSelect2: any = [
    {
      id: 1,
      text: "Client 1",
    },
    {
      id: 2,
      text: "Client 2",
    }
  ]

  constructor() { }

  /**
   * Client list
   */
  clientList() {
    return this.clients;
  }

  /**
   * Client list
   */
  clientListSelect2() {
    return this.clientsSelect2;
  }
}
