import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../../environments/environment';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ClientService } from '../client.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  clients: any = [];
  clientForm: FormGroup;
  actionTitle: string = 'Add Client';
  isEdit: boolean = false;
  isFormSubmitted: boolean = false;
  selectedClient: any = null;

  constructor(
    private title: Title,
    private ngxSmartModalService: NgxSmartModalService,
    private formBuilder: FormBuilder,
    private router: Router,
    private clientService: ClientService,
  ) { }

  /**
   * Set page title
   * Build form
   * Load client list
   */
  ngOnInit() {
    this.title.setTitle(`${environment.appTitle} - Clients`);
    this.buildForm();
    this.loadClientLists();
  }

  /**
   * Get form controls
   */
  get formControls() {
    return this.clientForm.controls;
  }

  /**
   * Build form
   */
  buildForm() {
    this.clientForm = this.formBuilder.group({
      name: [null, Validators.required],
      username: [null, Validators.required],
      email: [null, [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      status: ['active', Validators.required],
      id: [null],
    });
  }

  /**
   * Load client list
   */
  loadClientLists() {
    this.clients = this.clientService.clientList();
  }

  /**
   * Add/edit client
   * 
   * @param type
   * @param data
   */
  addOREdit(type: string, data: any = null) {
    if (type == 'add') {
      this.isEdit = false;
      this.actionTitle = 'Add Client';
    } else {
      this.isEdit = true;
      this.actionTitle = 'Edit Client';
      this.formControls.name.setValue(data.name);
      this.formControls.username.setValue(data.username);
      this.formControls.email.setValue(data.email);
      this.formControls.status.setValue(data.status);
      this.formControls.id.setValue(data.id);
    }
    this.ngxSmartModalService.open('addClient');
  }

  /**
   * Reset form
   */
  resetForm() {
    this.isFormSubmitted = false;
    this.clientForm.reset();
    this.ngxSmartModalService.close('addClient');
  }

  /**
   * Delete client
   * @param client 
   */
  delete(client: any) {
    this.selectedClient = client;
    this.ngxSmartModalService.open('deleteConfirm');
  }

  /**
   * Update client
   */
  updateClient() {
    this.isFormSubmitted = true;
    if (this.clientForm.invalid) {
      return;
    }
    let query = {
      name: this.formControls.name.value,
      username: this.formControls.username.value,
      email: this.formControls.email.value,
      status: this.formControls.status.value,
      id: this.formControls.id.value
    }

    var index = this.clients.findIndex(x => x.id == this.formControls.id.value);
    this.clients[index] = query;
    this.resetForm();
  }

  /**
   * Save client
   */
  saveClient() {
    this.isFormSubmitted = true;
    if (this.clientForm.invalid) {
      return;
    }
    let query = {
      name: this.formControls.name.value,
      username: this.formControls.username.value,
      email: this.formControls.email.value,
      status: this.formControls.status.value,
      id: Date.now()
    }
    this.clients.push(query);
    this.resetForm();
  }

  /**
   * Confirm delete
   */
  cancelDelete() {
    this.selectedClient = null;
    this.ngxSmartModalService.close('deleteConfirm');
  }

  /**
   * Confirm delete
   */
  confirmDelete() {
    if (this.selectedClient) {
      let index = this.clients.indexOf(this.selectedClient);
      if (index > -1) {
        this.clients.splice(index, 1);
      }
    }
    this.cancelDelete();
  }

  /**
   * Go to settings
   */
  goToSettings() {
    this.router.navigate(['settings'], { queryParams: { name: this.formControls.name.value } });
  }

}
