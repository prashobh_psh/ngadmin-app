import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClientRoutingModule } from './client-routing.module';
import { ListComponent } from './list/list.component';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    NgxSmartModalModule.forChild(),
    FormsModule,
    ReactiveFormsModule,
    ClientRoutingModule
  ],
  providers: []
})
export class ClientModule { }
