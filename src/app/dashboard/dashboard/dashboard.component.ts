import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private title: Title
  ) { }

  /**
   * Set page title
   */
  ngOnInit() {
    this.title.setTitle(`${environment.appTitle} - Dashboard`);
  }

}
